### Initialisation Code
# diffractio seems to have types for units, assuming np is just numpy
import numpy as np
from diffractio import um, nm, mm, degrees

import matplotlib.pyplot as plt

# Import classes corresponding to specific mathematical constructs
from diffractio.scalar_sources_XY import Scalar_source_XY
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY

# Constants of our system we will use later
wavelength = 0.6238*um
focus = 10*mm

# Defining XY grid to be calculated
# need a power of 2 in length for fft 
length = 500*um
x0 = np.linspace(-length, length, 512)
y0 = np.linspace(-length, length, 512)

import cv2


def detectRadius(image):
    """Find radius of zero area by edge detecting circle and computing area enclosed using opencv"""
    img = cv2.imread(image)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(image=img, threshold1=300, threshold2=800)
    contours,hierarchy = cv2.findContours(edges, 1, 2)
    return np.floor((np.sqrt(cv2.contourArea(contours[0])/np.pi)))



def performance(mask, nomask, r):
    isolate = Scalar_mask_XY(x=self.cg.x, y=self.cg.y, wavelength=self.cg.wavelength)
    isolate.circle(r0=(0*um, 0*um), radius= r*3.75*um)
    before = np.sum((nomask*isolate).intensity())
    after = np.sum((mask*isolate).intensity())
    return (before-after)/before


def coronagraphPerformance(dataNoCoronagraph, dataWithCoronagraph, pupilImageRadius):
    isolate = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
    isolate.circle(r0=(0*um, 0*um), radius=pupilImageRadius)
    before = np.sum((dataNoCoronagraph*isolate).intensity())
    after = np.sum((dataWithCoronagraph*isolate).intensity())
    return (before-after)/before




# ========== Generate Images ==========
exptDataNoVortex = Scalar_mask_XY(x=x0,y=y0, wavelength=wavelength)
exptDataNoVortex.image(filename="measured_no_OVC.bmp", normalize=True)


exptData = Scalar_mask_XY(x=x0,y=y0, wavelength=wavelength)
exptData.image(filename="measured_OVC.bmp", normalize=True)
print(len(exptData.u))

rad = detectRadius("measured_OVC.bmp")
print(rad)

# Generate Theoretical Plot
def coronagraph(r, bins, rad):
    if r<rad:
        return 0
    else:
        return 1/(r**2)
    
bins = 512
theoreticalData = np.zeros((bins,bins))

for i in range(bins):
    for j in range(bins):
        theoreticalData[i,j] = -coronagraph(np.sqrt((i-bins/2)**2 + (j-bins/2)**2), bins, rad)


        
# Calculate Numerical Plot
# Define Wave Source

rad_um = 2*length*rad/512 
src = Scalar_source_XY(x=x0, y=y0,wavelength=wavelength)
src.zernike_beam(A=1, r0=(0,0), radius=rad_um, n=[0], m=[0], c_nm=[1])

# Define lens
l1 = Scalar_mask_XY(x=x0,y=y0, wavelength=wavelength)
l1.lens(r0=(0*um,0*um), radius = 10000*mm, focal = focus, mask=False)


# Define Vortex Mask
v_globals = {'np' : np, 'l0' : wavelength}
vortex = Scalar_mask_XY(x=x0, y=y0,wavelength=wavelength)
vortex.mask_from_function(r0 = (0, 0),index=2, f1 = "0", f2 = "2*np.arctan2(self.X, self.Y)*l0/2/np.pi" ,radius = 10*mm,v_globals=v_globals ,mask = True)

u0 = (src*l1).RS(z=focus, new_field=True)
u1 = u0*vortex
u2 = u1.RS(z=focus, new_field=True)
simulData = (u2*l1).RS(z=2*focus, new_field=True)


# No Vortex Data
src = Scalar_source_XY(x=x0, y=y0,wavelength=wavelength)
src.zernike_beam(A=1, r0=(0,0), radius=250*um, n=[0], m=[0], c_nm=[1])
l1 = Scalar_mask_XY(x=x0,y=y0, wavelength=wavelength)
l1.lens(r0=(0*um,0*um), radius = 10000*mm, focal = focus, mask=False)
u0 = (src*l1).RS(z=focus, new_field=True)
u1 = u0
u2 = u1.RS(z=focus, new_field=True)
simulDataNoVortex = (u2*l1).RS(z=2*focus, new_field=True)
        
    


        
fig, axs = plt.subplots(1, 3, figsize=(9, 3))

axs[0].imshow(theoreticalData, cmap="Greys")
axs[0].axis('off')
#axs[0].set_title("Fourier Transform", fontsize=10)

axs[1].imshow(np.abs(simulData.u)*(-1), cmap="Greys")
axs[1].axis('off')
#axs[1].set_title("Rayleigh-Sommerfeld", fontsize=10)

axs[2].imshow(np.abs(exptData.u)*(-1), cmap="Greys")
axs[2].axis('off')
#axs[2].set_title("Experimental Measurment", fontsize=10)

plt.savefig('filename.png', dpi=500)
plt.show()





# ========== Generate Intensity Cross-Sections ==========
fig, axs = plt.subplots(1, 3, figsize=(9, 3))

axs[0].plot((-1)*theoreticalData[256][:], color="black")
axs[0].axis("off")
#axs[0].vlines(256+rad, 0, -np.min(theoreticalData)*1.1, color="red", linestyle="dashed", alpha=0.75)
#axs[0].vlines(256-rad, 0, -np.min(theoreticalData)*1.1, color="red", linestyle="dashed", alpha=0.75)

data0 = np.abs(simulData.u[256][:])
data1 = np.abs(simulDataNoVortex.u[256][:])
#axs[1].plot(data1, color="lightgrey")
axs[1].plot(data0, color="black")
axs[1].axis("off")
#axs[1].vlines(np.floor(512/2 +rad), 0, np.max(data1), color="red", linestyle="dashed", alpha=0.75)
#axs[1].vlines(np.floor(512/2 -rad), 0, np.max(data1), color="red", linestyle="dashed", alpha=0.75)


data0 = np.abs(exptData.u[256][:])
data1 = np.abs(exptDataNoVortex.u[256][:])
#axs[2].plot(data1, color="lightgrey")
axs[2].plot(data0, color="black")
axs[2].axis("off")
#axs[2].vlines(np.floor(512/2)+rad, 0, 1, color="red", linestyle="dashed", alpha=0.75)
#axs[2].vlines(np.floor(512/2)-rad, 0, 1, color="red", linestyle="dashed", alpha=0.75)

#axs[0].set_title("Analytical Fourier Transform", fontsize=10)
#axs[1].set_title("Rayleigh-Sommerfeld", fontsize=10)
#axs[2].set_title("Experimental Measurment", fontsize=10)

plt.savefig('profiles.png', dpi=500)
plt.show()







# ========== Print Performance Numbers ==========
print("Analytical Performance = 100%")

RSPerformance = round(coronagraphPerformance(simulDataNoVortex, simulData, rad_um), 3)
print("RS Performance = " + str(RSPerformance*100) + "%")

EXPTPerformance = round(coronagraphPerformance(exptDataNoVortex,exptData,rad_um), 3)*100
print("Experimental Performance = >" + str(EXPTPerformance) + "%")